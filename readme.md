
# MiddleManApp/Slim-template

http://MiddleManApp.com/basics/directory-structure SSG(*) CMS starter kit with https://github.com/Slim-template/Slim#syntax-example for templating (html generation. Short tutorial on Slim: instead of writing the opening *AND* closing tags, such as 'body' tag, you just write the tag once; Slim closes it for you. This comes in handy in eliminating bugs for larger web app|site since it gives you errors at edit time). Of course, you can make base 'layouts' and 'include partials' (ex 'include source/layouts/_header' ). I looked at a many CMS apps, and chose MiddleManApp as the CMS to use as having least friction. 


SSG(*) acronym stands for 'generated', or even JAMstack) content is generated at the 'edit' stage, ahead of use. Compared to SSR(ex Wordpress) that 'generates' at runtime: when the user views page|content. SSG needs all the data to be there ahead of use (else it has to use fetch|API or REST calls for any dynamic data), for example a JSON file list of items, such as blog posts, employees, etc. SSG is much better at SEO, and therefore it is better at content marketing. However, you will have to at least glance the 'Slim' doc linked above - so there is something that you have to learn to use this starter kit. Think of Slim as a more powerful version of Markdown. Markdown only does some html tags and very little CSS: Slim does *ANYTHING* html can. 


In source/assets folder you'll see some sample fonts you could chose to use, and some js libs that you chose chose to use. *PLUS*: 3 example CSS frameworks. Pick a CSS framework that will make it easy for you to maintain your web app|site. Delete any files you don't plan to use, in the source folder, since build is generated from the source folder. Last, the shell file (b.sh) starts a 'watcher' that auto reloads the browser as you edit. (you do have to type in your actual IP in config.rb for the browser reload to work).



## Notes:
- Tested w/ current Ruby v3.2
- New version will be using Bulma css
- Ideal way to use this is to edit in the cloud and not edit on your local PC or laptop.
- Another, older starter kit: http://github.com/rriemann/MiddleMan-blog-Template-duocolor
