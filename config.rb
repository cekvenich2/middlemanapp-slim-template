
require 'slim'
set :slim, { pretty: true }
require 'slim/include'
require 'slim/smart'
# this next line if enabled breaks variables!!:
#require 'slim/logic_less'

activate :i18n

# this IP has to match your server
activate :livereload, :host => "154.12.250.248"

